unit input_stock_barang;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, DB, ADODB;

type
  Tform_input_stock_barang = class(TForm)
    Label1: TLabel;
    StaticText1: TStaticText;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    Label9: TLabel;
    EJumlah: TEdit;
    BSave: TBitBtn;
    CBKodeBarang: TComboBox;
    Q_barang: TADOQuery;
    ADOConnection1: TADOConnection;
    q_stock: TADOQuery;
    ado_barang: TADOTable;
    ado_stock: TADOTable;
    ado_barangkode_barang: TStringField;
    ado_barangnama_barang: TStringField;
    ado_barangbrand: TStringField;
    ado_barangjenis_barang: TStringField;
    ado_barangukuran: TIntegerField;
    ado_barangwarna: TStringField;
    ado_barangharga: TIntegerField;
    ado_stockkode_barang: TStringField;
    ado_stockjumlah: TIntegerField;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    enama: TEdit;
    ebrand: TEdit;
    ejenis: TEdit;
    ewarna: TEdit;
    eharga: TEdit;
    procedure BCloseClick(Sender: TObject);
    procedure BSaveClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CBKodeBarangChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  form_input_stock_barang: Tform_input_stock_barang;

implementation

{$R *.dfm}

procedure Tform_input_stock_barang.BCloseClick(Sender: TObject);
begin
close;
end;

procedure Tform_input_stock_barang.BSaveClick(Sender: TObject);
begin

ado_stock.Edit;
ado_stock['kode_barang']:=CBKodeBarang.Text;
ado_stock['jumlah']:=EJumlah.Text;
ado_stock.Post;
ShowMessage('data telah terubah');
CBKodeBarang.ItemIndex:=0;
EJumlah.Clear;
EJumlah. SetFocus;
q_stock.Close;
q_stock.Open;

ShowMessage('DATA TELAH TERINPUT TERIMAKASIH!');
close;
end;

procedure Tform_input_stock_barang.FormCreate(Sender: TObject);


 var i:Integer;
 begin
  Q_barang.First;
  for i:=1 to Q_barang.RecordCount do
begin
  CBKodeBarang.Items.add(Q_barang['kode_barang']);
  Q_barang.Next;

end;

end;

procedure Tform_input_stock_barang.CBKodeBarangChange(Sender: TObject);
begin
      q_stock.Close;
      q_stock.SQL.Clear;
      Q_barang.Close;
      Q_barang.SQL.Clear;
      q_stock.SQL.Text:='Select * from t_stock where kode_barang=''' +CBKodeBarang.Text+'''';
      Q_barang.SQL.Text:='Select * from t_barang where kode_barang=''' +CBKodeBarang.Text+'''';
      q_stock.Open;
      Q_barang.Open;

  EJumlah.Text:=q_stock['jumlah'];
  enama.Text:=Q_barang['nama_barang'];
  ebrand.Text:=Q_barang['brand'];
  ejenis.Text:=Q_barang['jenis_barang'];
  ewarna.Text:=Q_barang['warna'];
  eharga.Text:=Q_barang['harga'];
end;

end.
