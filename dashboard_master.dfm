object form_dashboard_master: Tform_dashboard_master
  Left = 492
  Top = 235
  Width = 940
  Height = 468
  Caption = 'form_dashboard_master'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShlaow
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 8
    Top = 8
    Width = 71
    Height = 19
    Caption = 'Username'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 88
    Top = 8
    Width = 99
    Height = 19
    Caption = '11123121231'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label1: TLabel
    Left = 264
    Top = 8
    Width = 63
    Height = 19
    Caption = 'Tanggal:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 336
    Top = 8
    Width = 6
    Height = 19
    Caption = '-'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object ActionMainMenuBar1: TActionMainMenuBar
    Left = 488
    Top = 0
    Width = 433
    Height = 29
    UseSystemFont = False
    ActionManager = ActionManager1
    Align = alCustom
    BiDiMode = bdRightToLeft
    Caption = 'ActionMainMenuBar1'
    ColorMap.HighlightColor = clWhite
    ColorMap.BtnSelectedColor = clBtnFace
    ColorMap.UnusedColor = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    HorzMargin = 5
    ParentBiDiMode = False
    Spacing = 20
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 410
    Width = 924
    Height = 19
    Panels = <>
    SimpleText = 'Lorem'
  end
  object Panel1: TPanel
    Left = 8
    Top = 64
    Width = 345
    Height = 41
    TabOrder = 2
    object Label5: TLabel
      Left = 8
      Top = 8
      Width = 188
      Height = 19
      Caption = 'Total Pemasukan Hari ini :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 216
      Top = 8
      Width = 99
      Height = 19
      Caption = '11123121231'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
  end
  object Panel2: TPanel
    Left = 368
    Top = 64
    Width = 345
    Height = 41
    TabOrder = 3
    object Label7: TLabel
      Left = 8
      Top = 8
      Width = 164
      Height = 19
      Caption = 'Total Transaksi Hari ini'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel
      Left = 208
      Top = 8
      Width = 99
      Height = 19
      Caption = '11123121231'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
  end
  object DBGrid1: TDBGrid
    Left = 16
    Top = 168
    Width = 716
    Height = 169
    DataSource = DataSource1
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object Button2: TButton
    Left = 16
    Top = 128
    Width = 153
    Height = 25
    Caption = 'Lihat Transaksi Hari ini  '
    TabOrder = 5
    OnClick = Button2Click
  end
  object Panel3: TPanel
    Left = 16
    Top = 344
    Width = 345
    Height = 41
    TabOrder = 6
    object Label9: TLabel
      Left = 8
      Top = 8
      Width = 174
      Height = 19
      Caption = 'Total Semua Pemasukan'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label10: TLabel
      Left = 216
      Top = 8
      Width = 99
      Height = 19
      Caption = '11123121231'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
  end
  object Panel4: TPanel
    Left = 384
    Top = 344
    Width = 345
    Height = 41
    TabOrder = 7
    object Label11: TLabel
      Left = 8
      Top = 8
      Width = 167
      Height = 19
      Caption = 'Total Seluruh Transaksi'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label12: TLabel
      Left = 216
      Top = 8
      Width = 99
      Height = 19
      Caption = '11123121231'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
  end
  object ActionManager1: TActionManager
    ActionBars = <
      item
        Items = <
          item
            Action = Action8
            Caption = '&Logout'
          end
          item
            Items = <
              item
                Action = Action5
                Caption = '&Stock Barang'
              end
              item
                Action = Action6
                Caption = '&User'
              end
              item
                Action = Action7
                Caption = '&Artikel'
              end>
            Caption = '&Input'
          end
          item
            Items = <
              item
                Action = Action2
                Caption = '&Data Stock Barang'
              end
              item
                Action = Action3
                Caption = 'D&ata User'
              end
              item
                Action = Action4
                Caption = 'Da&ta Retur'
              end
              item
                Action = Action1
                Caption = 'Data T&ransaksi'
              end>
            Caption = '&Report'
          end>
        ActionBar = ActionMainMenuBar1
      end>
    Left = 224
    StyleName = 'XP Style'
    object Action2: TAction
      Category = 'Report'
      Caption = 'Data Stock Barang'
      OnExecute = Action2Execute
    end
    object Action3: TAction
      Category = 'Report'
      Caption = 'Data User'
      OnExecute = Action3Execute
    end
    object Action4: TAction
      Category = 'Report'
      Caption = 'Data Retur'
      OnExecute = Action4Execute
    end
    object Action5: TAction
      Category = 'Input'
      Caption = 'Stock Barang'
      OnExecute = Action5Execute
    end
    object Action6: TAction
      Category = 'Input'
      Caption = 'User'
      OnExecute = Action6Execute
    end
    object Action7: TAction
      Category = 'Input'
      Caption = 'Artikel'
      OnExecute = Action7Execute
    end
    object Action8: TAction
      Caption = 'Logout'
      OnExecute = Action8Execute
    end
    object Action1: TAction
      Category = 'Report'
      Caption = 'Data Transaksi'
      OnExecute = Action1Execute
    end
  end
  object ADOConnection1: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=root;Data' +
      ' Source=uas_provis;Initial Catalog=db_uas_provis'
    LoginPrompt = False
    Left = 72
    Top = 368
  end
  object ADOQuery2: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    Left = 192
    Top = 368
  end
  object ADOQuery1: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    Left = 232
    Top = 368
  end
  object DataSource1: TDataSource
    DataSet = ADOQuery3
    Left = 152
    Top = 368
  end
  object ADOConnection2: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=root;Data' +
      ' Source=uas_provis;Initial Catalog=db_uas_provis'
    LoginPrompt = False
    Left = 72
    Top = 368
  end
  object ADOQuery3: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    Left = 272
    Top = 368
  end
  object ADOQuery4: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    Left = 320
    Top = 368
  end
  object ADOQuery5: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    Left = 368
    Top = 368
  end
end
