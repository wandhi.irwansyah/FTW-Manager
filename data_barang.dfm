object form_data_barang: Tform_data_barang
  Left = 186
  Top = 175
  Width = 958
  Height = 446
  Caption = 'form_data_barang'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 56
    Top = 16
    Width = 143
    Height = 24
    Caption = 'Data Stok Barang'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 384
    Top = 88
    Width = 34
    Height = 13
    Caption = 'Search'
  end
  object DBGrid1: TDBGrid
    Left = 16
    Top = 152
    Width = 865
    Height = 209
    DataSource = ds_barang
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object Button1: TButton
    Left = 48
    Top = 88
    Width = 75
    Height = 25
    Caption = 'Tambah'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button3: TButton
    Left = 136
    Top = 88
    Width = 75
    Height = 25
    Caption = 'Delete'
    TabOrder = 2
  end
  object ComboBox1: TComboBox
    Left = 432
    Top = 88
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 3
    Text = 'kode_barang'
    Items.Strings = (
      'kode_barang'
      'nama_barang'
      'brand'
      'jenis_barang'
      'ukuran'
      'warna')
  end
  object e_cari: TEdit
    Left = 592
    Top = 88
    Width = 201
    Height = 21
    TabOrder = 4
  end
  object Button4: TButton
    Left = 800
    Top = 88
    Width = 75
    Height = 25
    Caption = 'CARI'
    TabOrder = 5
    OnClick = Button4Click
  end
  object c_barang: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=root;Data' +
      ' Source=uas_provis;Initial Catalog=db_uas_provis'
    LoginPrompt = False
    Left = 336
    Top = 24
  end
  object q_barang: TADOQuery
    Connection = c_barang
    CursorType = ctStatic
    Parameters = <>
    Left = 368
    Top = 24
  end
  object ds_barang: TDataSource
    DataSet = q_barang
    Left = 400
    Top = 24
  end
end
