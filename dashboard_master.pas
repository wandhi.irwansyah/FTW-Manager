unit dashboard_master;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, StdCtrls, ExtCtrls, ActnList, ToolWin, ActnMan,
  ActnCtrls, ActnMenus, XPStyleActnCtrls, Menus, ComCtrls, DB, ADODB;

type
  Tform_dashboard_master = class(TForm)
    ActionManager1: TActionManager;
    ActionMainMenuBar1: TActionMainMenuBar;
    Label2: TLabel;
    Label3: TLabel;
    Action2: TAction;
    Action3: TAction;
    Action4: TAction;
    Action5: TAction;
    Action6: TAction;
    Action7: TAction;
    Action8: TAction;
    Action1: TAction;
    StatusBar1: TStatusBar;
    Label1: TLabel;
    Label4: TLabel;
    Panel1: TPanel;
    Label5: TLabel;
    Label6: TLabel;
    ADOConnection1: TADOConnection;
    ADOQuery2: TADOQuery;
    Panel2: TPanel;
    Label7: TLabel;
    Label8: TLabel;
    ADOQuery1: TADOQuery;
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    ADOConnection2: TADOConnection;
    ADOQuery3: TADOQuery;
    Button2: TButton;
    Panel3: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    ADOQuery4: TADOQuery;
    Panel4: TPanel;
    Label11: TLabel;
    Label12: TLabel;
    ADOQuery5: TADOQuery;
    procedure Button3Click(Sender: TObject);
    procedure b_r_transaksiClick(Sender: TObject);
    procedure b_r_barangClick(Sender: TObject);
    procedure b_r_userClick(Sender: TObject);
    procedure b_r_returClick(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
    procedure Action2Execute(Sender: TObject);
    procedure Action3Execute(Sender: TObject);
    procedure Action4Execute(Sender: TObject);
    procedure Action5Execute(Sender: TObject);
    procedure Action6Execute(Sender: TObject);
    procedure Action7Execute(Sender: TObject);
    procedure Action8Execute(Sender: TObject);
    procedure FormShlaow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    i:integer;
  end;

var
  form_dashboard_master: Tform_dashboard_master;

implementation

uses login, data_barang, data_retur, data_transaksi, data_user,
  input_artikel, input_retur, input_stock_barang, input_user;

{$R *.dfm}

procedure Tform_dashboard_master.Button3Click(Sender: TObject);
begin
  form_login.Show;
  Close;
end;

procedure Tform_dashboard_master.b_r_transaksiClick(Sender: TObject);
begin
  form_data_transaksi.Show;
end;

procedure Tform_dashboard_master.b_r_barangClick(Sender: TObject);
begin
  form_data_barang.Show;
end;

procedure Tform_dashboard_master.b_r_userClick(Sender: TObject);
begin
form_data_user.Show;
end;

procedure Tform_dashboard_master.b_r_returClick(Sender: TObject);
begin
form_data_retur.Show;
end;

procedure Tform_dashboard_master.Button4Click(Sender: TObject);
begin
form_input_stock_barang.Show;
end;

procedure Tform_dashboard_master.Button5Click(Sender: TObject);
begin
  form_input_user.Show;
end;

procedure Tform_dashboard_master.Button6Click(Sender: TObject);
begin
form_input_artikel.Show;
end;

procedure Tform_dashboard_master.Action1Execute(Sender: TObject);
begin
  form_data_transaksi.Show;
end;

procedure Tform_dashboard_master.Action2Execute(Sender: TObject);
begin
form_data_barang.Show;
end;

procedure Tform_dashboard_master.Action3Execute(Sender: TObject);
begin
form_data_user.Show;
end;

procedure Tform_dashboard_master.Action4Execute(Sender: TObject);
begin
form_data_retur.Show;
end;

procedure Tform_dashboard_master.Action5Execute(Sender: TObject);
begin
form_input_stock_barang.Show;
end;

procedure Tform_dashboard_master.Action6Execute(Sender: TObject);
begin
form_input_user.Show;
end;

procedure Tform_dashboard_master.Action7Execute(Sender: TObject);
begin
form_input_artikel.Show;
end;

procedure Tform_dashboard_master.Action8Execute(Sender: TObject);
begin
form_login.Visible :=true;
Close;
end;

procedure Tform_dashboard_master.FormShlaow(Sender: TObject);
begin
    i:=0;
   ADOQuery2.SQL.Clear;
   ADOQuery2.SQL.Text:='Select *,SUM(total) AS sum from t_transaksi where tgl_transaksi='''+formatdatetime('yyyy-mm-dd',now)+'''';
   ADOQuery2.Open;
    ADOQuery1.SQL.Clear;
   ADOQuery1.SQL.Text:='Select *,COUNT(kode_transaksi) AS count from t_transaksi where tgl_transaksi='''+formatdatetime('yyyy-mm-dd',now)+'''';
   ADOQuery1.Open;
    ADOQuery3.SQL.Clear;
   ADOQuery3.SQL.Text:='Select * from t_transaksi where tgl_transaksi='''+formatdatetime('yyyy-mm-dd',now)+'''';
   ADOQuery3.Open;
   ADOQuery4.SQL.Clear;
   ADOQuery4.SQL.Text:='Select *,SUM(total) AS sum from t_transaksi';
   ADOQuery4.Open;
   ADOQuery5.SQL.Clear;
   ADOQuery5.SQL.Text:='Select  *,COUNT(kode_transaksi) AS count from t_transaksi';
   ADOQuery5.Open;
  label4.Caption :=formatdatetime('yyyy-mm-dd',now);
  label6.Caption := ADOQuery2['sum'];
  label8.Caption := ADOQuery1['count'];
  label10.Caption := ADOQuery4['sum'];
  label12.Caption := ADOQuery5['count'];

end;

procedure Tform_dashboard_master.Button1Click(Sender: TObject);
begin
 ADOQuery3.Close;
 ADOQuery3.SQL.Clear;
   ADOQuery3.SQL.Text:='Select * from t_transaksi';
   ADOQuery3.Open;
end;

procedure Tform_dashboard_master.Button2Click(Sender: TObject);
begin
  if(i = 0)then
  begin
  i := 1;
  Button2.Caption := 'Lihat Transaksi Hari Ini';
 ADOQuery3.Close;
 ADOQuery3.SQL.Clear;
     ADOQuery3.SQL.Text:='Select * from t_transaksi';
   ADOQuery3.Open;
   end
   else
    begin
    i := 0;
    Button2.Caption := 'Lihat semua Transaksi';
    ADOQuery3.Close;
 ADOQuery3.SQL.Clear;

   ADOQuery3.SQL.Text:='Select * from t_transaksi where tgl_transaksi='''+formatdatetime('yyyy-mm-dd',now)+'''';
   ADOQuery3.Open;
    end;
end;

end.
