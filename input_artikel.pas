unit input_artikel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, DB, ADODB;

type
  Tform_input_artikel = class(TForm)
    Label1: TLabel;
    StaticText1: TStaticText;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EJumlah: TEdit;
    BSave: TBitBtn;
    BClose: TBitBtn;
    ENamaBarang: TEdit;
    EBrand: TEdit;
    EJenisBarang: TEdit;
    EUkuran: TEdit;
    EWarna: TEdit;
    ADOConnection1: TADOConnection;
    Label2: TLabel;
    EHarga: TEdit;
    ado_t_barang: TADOTable;
    ado_t_stock: TADOTable;
    ado_t_barangkode_barang: TStringField;
    ado_t_barangnama_barang: TStringField;
    ado_t_barangbrand: TStringField;
    ado_t_barangjenis_barang: TStringField;
    ado_t_barangukuran: TIntegerField;
    ado_t_barangwarna: TStringField;
    ado_t_barangharga: TIntegerField;
    ado_t_stockkode_barang: TStringField;
    ado_t_stockjumlah: TIntegerField;
    procedure BSaveClick(Sender: TObject);
    procedure BCloseClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  form_input_artikel: Tform_input_artikel;

implementation

uses StrUtils, data_barang;

{$R *.dfm}

procedure Tform_input_artikel.BSaveClick(Sender: TObject);
var
artikel : string;

begin
artikel:=(LeftStr(EBrand.Text, 3))+ LeftStr(ENamaBarang.Text, 3)+(EUkuran.Text);
{ADOQuery1.Close;
ADOQuery1.SQL.Clear;
ADOQuery1.SQL.Add('insert into t_barang values ("'+artikel+'", "'+ENamaBarang.Text+'", "'+EBrand.Text+'", "'+EJenisBarang.Text+'", "'+EUkuran.Text+'", "'+EWarna.Text+'", "'+EHarga.Text+'")');
ADOQuery1.ExecSQL;
ADOQuery1.SQL.Clear;
ADOQuery1.SQL.Add('insert into t_stock values ("'+artikel+'", "'+EJumlah.Text+'")');
ADOQuery1.ExecSQL;}
ado_t_barang.Open;
ado_t_stock.Open;
if ado_t_barang['kode_barang']=artikel then

begin
ado_t_stock.Edit;
ado_t_stock['jumlah']:=(ado_t_stock['jumlah'])+StrToInt(EJumlah.Text);
ado_t_stock.Post;
ShowMessage('Data Sudah ada, Jumlah Barang sudah di perbaharui!!!');
end
else
if ado_t_barang['kode_barang']<>artikel then
begin
ado_t_barang.Append;
ado_t_stock.Append;
ado_t_barang['kode_barang']:=artikel;
ado_t_barang['nama_barang']:=ENamaBarang.Text;
ado_t_barang['brand']:=EBrand.Text;
ado_t_barang['jenis_barang']:= EJenisBarang.Text;
ado_t_barang['ukuran']:=EUkuran.Text;
ado_t_barang['warna']:=EWarna.Text;
ado_t_barang['harga']:=EHarga.Text;
ado_t_stock['jumlah']:=EJumlah.Text;
ado_t_stock['kode_barang']:=artikel;
ado_t_barang.Post;
ado_t_stock.Post;
ShowMessage('Data Baru Telah Masuk');
end;
ENamaBarang.Clear;
EBrand.Clear;
EJenisBarang.Clear;
EUkuran.Clear;
EWarna.Clear;
EHarga.Clear;
EJumlah.Clear;

ENamaBarang.Clear;
EBrand.Clear;
EJenisBarang.Clear;
EUkuran.Clear;
EWarna.Clear;
EHarga.Clear;
EJumlah.Clear;



end;

procedure Tform_input_artikel.BCloseClick(Sender: TObject);
begin                      
  form_data_barang.Show;
  form_input_artikel.Close;
end;

end.
