unit data_user;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, ExtCtrls, DBCtrls, StdCtrls, Grids, DBGrids, Buttons;

type
  Tform_data_user = class(TForm)
    Enik: TEdit;
    ADOConnection1: TADOConnection;
    ADOlogin: TADOQuery;
    GroupBox1: TGroupBox;
    DBGrid1: TDBGrid;
    nik: TLabel;
    Label1: TLabel;
    Label4: TLabel;
    DataSource1: TDataSource;
    ComboBox1: TComboBox;
    BitBtn1: TBitBtn;
    Button1: TButton;
    Button4: TButton;
    ADOTable1: TADOTable;
    ADOTable1nik: TStringField;
    ADOTable1nama: TStringField;
    ADOTable1alamat: TStringField;
    ADOTable1telepon: TStringField;
    ADOTable1level: TIntegerField;
    ADOTable1status: TIntegerField;
    ADOTable1password: TStringField;
    procedure BitBtn1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  form_data_user: Tform_data_user;

implementation

uses input_user;

{$R *.dfm}

procedure Tform_data_user.BitBtn1Click(Sender: TObject);
begin
form_input_user.Show;
end;

procedure Tform_data_user.Button1Click(Sender: TObject);
begin
    if(Enik.Text='')then
    else
  begin
    ADOlogin.Close;
    ADOlogin.SQL.Clear;
   ADOlogin.SQL.Text:='Select * from t_user where nik='''+Enik.Text+'''';
   ADOlogin.Open;
    if(ADOlogin.RecordCount=0) then
    showmessage('Data Tidak Ditemukan');

end;
end;

procedure Tform_data_user.Button4Click(Sender: TObject);
var
  hasil, query : String;
begin
  if(combobox1.Text = 'telepon')then
  begin
  query := 'select * from t_user Order By telepon ASC';
  end;
  if(combobox1.Text = 'nik')then
  begin
  query := 'select * from t_user Order By nik ASC';
  end;
  if(combobox1.Text = 'alamat')then
  begin
  query := 'select * from t_user Order By alamat ASC';
  end;
 ADOlogin.Close;
    ADOlogin.SQL.Clear;
   ADOlogin.SQL.Text:=query;
ADOlogin.Open;
end;

procedure Tform_data_user.FormShow(Sender: TObject);
begin
    ADOlogin.Close;
    ADOlogin.SQL.Clear;
    ADOlogin.SQL.Add('select * from t_user');
    ADOlogin.Open;
end;

end.
