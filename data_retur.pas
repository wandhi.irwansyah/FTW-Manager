unit data_retur;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, ExtCtrls, DBCtrls, Grids, DBGrids, Buttons, StdCtrls;

type
  Tform_data_retur = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Button1: TButton;
    ComboBox1: TComboBox;
    Ekoderetur: TEdit;
    GroupBox1: TGroupBox;
    DBGrid1: TDBGrid;
    ADOConnection1: TADOConnection;
    ADOQuery1: TADOQuery;
    DataSource1: TDataSource;
    cari: TButton;
    procedure BitBtn4Click(Sender: TObject);
    procedure cariClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  form_data_retur: Tform_data_retur;

implementation

uses input_retur;

{$R *.dfm}

procedure Tform_data_retur.BitBtn4Click(Sender: TObject);
begin
    form_input_retur.show;
end;

procedure Tform_data_retur.cariClick(Sender: TObject);
begin
    if(Ekoderetur.Text='')then
    else
  begin
    ADOquery1.Close;
    ADOquery1.SQL.Clear;
    ADOquery1.SQL.Add('select * from t_retur where kode_retur='+quotedstr(Ekoderetur.Text));
    ADOquery1.Open;
    if(ADOquery1.RecordCount=0) then
    showmessage('Data Tidak Ditemukan');

end;
end;

procedure Tform_data_retur.Button1Click(Sender: TObject);
var
hasil,siquery:String;
begin
  case ComboBox1.ItemIndex of
    0:hasil := 'kode_barang';
    1:hasil := 'kode_retur';
    end;
  siquery := 'select * from t_retur Order By ' + hasil + ' DESC';
ADOQuery1.Close;
ADOQuery1.SQL.Clear;
ADOQuery1.SQL.Add(siquery);
ADOQuery1.ExecSQL;
ADOQuery1.Open;
end;

end.
