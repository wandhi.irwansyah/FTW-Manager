object form_input_retur: Tform_input_retur
  Left = 267
  Top = 113
  Width = 878
  Height = 544
  Caption = 'form_input_retur'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 200
    Top = 112
    Width = 296
    Height = 16
    Caption = 'Digunakan untuk penginputan retur barang '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object StaticText1: TStaticText
    Left = 200
    Top = 72
    Width = 444
    Height = 43
    Caption = 'Form Aplikasi Retur Barang'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -32
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
  end
  object GroupBox1: TGroupBox
    Left = 200
    Top = 128
    Width = 441
    Height = 305
    Caption = 'Masukan Data Barang Retur'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    object jumlah: TLabel
      Left = 40
      Top = 152
      Width = 93
      Height = 16
      Caption = 'Jumlah          : '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
    end
    object keterangan: TLabel
      Left = 40
      Top = 184
      Width = 93
      Height = 16
      Caption = 'Keterangan  : '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
    end
    object kodebarang: TLabel
      Left = 40
      Top = 120
      Width = 95
      Height = 16
      Caption = 'Kode Barang : '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
    end
    object Ejumlah: TEdit
      Left = 144
      Top = 144
      Width = 225
      Height = 27
      TabOrder = 0
    end
    object Eketerangan: TEdit
      Left = 144
      Top = 176
      Width = 225
      Height = 27
      TabOrder = 1
    end
    object BitBtn1: TBitBtn
      Left = 56
      Top = 256
      Width = 65
      Height = 25
      Caption = 'Save'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = BitBtn1Click
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333330070
        7700333333337777777733333333008088003333333377F73377333333330088
        88003333333377FFFF7733333333000000003FFFFFFF77777777000000000000
        000077777777777777770FFFFFFF0FFFFFF07F3333337F3333370FFFFFFF0FFF
        FFF07F3FF3FF7FFFFFF70F00F0080CCC9CC07F773773777777770FFFFFFFF039
        99337F3FFFF3F7F777F30F0000F0F09999937F7777373777777F0FFFFFFFF999
        99997F3FF3FFF77777770F00F000003999337F773777773777F30FFFF0FF0339
        99337F3FF7F3733777F30F08F0F0337999337F7737F73F7777330FFFF0039999
        93337FFFF7737777733300000033333333337777773333333333}
      NumGlyphs = 2
    end
    object ekodebarang: TComboBox
      Left = 144
      Top = 112
      Width = 225
      Height = 27
      ItemHeight = 19
      TabOrder = 3
    end
    object Button1: TButton
      Left = 328
      Top = 248
      Width = 75
      Height = 25
      Caption = 'Close'
      TabOrder = 4
      OnClick = Button1Click
    end
  end
  object ADOConnection1: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=root;Data' +
      ' Source=uas_provis;Initial Catalog=db_uas_provis'
    LoginPrompt = False
    Left = 112
    Top = 192
  end
  object ADOreturbarang: TADOQuery
    Connection = ADOConnection1
    Parameters = <>
    Left = 112
    Top = 224
  end
  object ADOTable1: TADOTable
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    TableName = 't_barang'
    Left = 112
    Top = 256
    object ADOTable1kode_barang: TStringField
      FieldName = 'kode_barang'
    end
    object ADOTable1nama_barang: TStringField
      FieldName = 'nama_barang'
    end
    object ADOTable1brand: TStringField
      FieldName = 'brand'
    end
    object ADOTable1jenis_barang: TStringField
      FieldName = 'jenis_barang'
    end
    object ADOTable1ukuran: TIntegerField
      FieldName = 'ukuran'
    end
    object ADOTable1warna: TStringField
      FieldName = 'warna'
    end
    object ADOTable1harga: TIntegerField
      FieldName = 'harga'
    end
  end
  object ADOTable2: TADOTable
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    TableName = 't_retur'
    Left = 112
    Top = 288
  end
end
