unit input_retur;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, DB, ADODB, ComCtrls, ExtCtrls;

type
  Tform_input_retur = class(TForm)
    Label1: TLabel;
    StaticText1: TStaticText;
    GroupBox1: TGroupBox;
    jumlah: TLabel;
    keterangan: TLabel;
    Ejumlah: TEdit;
    Eketerangan: TEdit;
    BitBtn1: TBitBtn;
    ADOConnection1: TADOConnection;
    ADOreturbarang: TADOQuery;
    kodebarang: TLabel;
    ekodebarang: TComboBox;
    ADOTable1: TADOTable;
    ADOTable1kode_barang: TStringField;
    ADOTable1nama_barang: TStringField;
    ADOTable1brand: TStringField;
    ADOTable1jenis_barang: TStringField;
    ADOTable1ukuran: TIntegerField;
    ADOTable1warna: TStringField;
    ADOTable1harga: TIntegerField;
    Button1: TButton;
    ADOTable2: TADOTable;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  form_input_retur: Tform_input_retur;

implementation

uses input_user, dashboard;

{$R *.dfm}

procedure Tform_input_retur.BitBtn1Click(Sender: TObject);
var
  tanggalretur, koderetur, tgl, waktu : String;
begin
    tgl := (FormatDateTime('yyyymmdd',now));
    waktu := (FormatDateTime('hhmmss',now));
    koderetur := 'rtr' + ekodebarang.Text + tgl + waktu;
    tanggalretur := (FormatDateTime('yyyy-mm-dd',now));

    ADOTable2.Open;
    ADOTable2.Append;
    ADOTable2['kode_retur']:=koderetur;
    ADOTable2['tgl_retur']:=tanggalretur;
    ADOTable2['kode_barang']:=ekodebarang.Text;
    ADOTable2['jumlah']:=Ejumlah.Text;
    ADOTable2['status']:='1';
    ADOTable2['keterangan']:=Eketerangan.Text;
    ADOTable2.Post;
    ekodebarang.ItemIndex:=-1;
    Ejumlah.Clear;
    Eketerangan.Clear;
    ShowMessage('Data Udah Ke Simpen Yapss');
    ekodebarang.SetFocus;

end;

procedure Tform_input_retur.FormShow(Sender: TObject);
var
 a : Integer;
begin
ADOTable1.First;
while not ADOTable1.Eof do
begin
       ekodebarang.Items.Add(ADOTable1['kode_barang']);
       ADOTable1.Next;
end;
end;

procedure Tform_input_retur.Button1Click(Sender: TObject);
begin
  form_dashboard.Show;
  Close;
end;

end.


