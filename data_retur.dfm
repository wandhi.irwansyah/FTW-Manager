object form_data_retur: Tform_data_retur
  Left = 157
  Top = 114
  Width = 930
  Height = 479
  Caption = 'form_data_retur'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 208
    Top = 120
    Width = 131
    Height = 13
    Caption = 'Search By Kode Retur : '
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 208
    Top = 56
    Width = 164
    Height = 29
    Caption = 'FTW Manager'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 192
    Top = 80
    Width = 3
    Height = 13
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 208
    Top = 88
    Width = 139
    Height = 13
    Caption = 'Tabel Data Barang Retur'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Button1: TButton
    Left = 560
    Top = 112
    Width = 49
    Height = 25
    Caption = 'Sort By'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnClick = Button1Click
  end
  object ComboBox1: TComboBox
    Left = 616
    Top = 112
    Width = 81
    Height = 21
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ItemHeight = 13
    ItemIndex = 0
    ParentFont = False
    TabOrder = 1
    Text = 'Kode Barang'
    Items.Strings = (
      'Kode Barang'
      'Kode Retur')
  end
  object Ekoderetur: TEdit
    Left = 344
    Top = 112
    Width = 113
    Height = 21
    TabOrder = 2
  end
  object GroupBox1: TGroupBox
    Left = 208
    Top = 144
    Width = 489
    Height = 137
    Caption = 'Dashboard Data Retur'
    Color = clGradientActiveCaption
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 3
  end
  object DBGrid1: TDBGrid
    Left = 216
    Top = 160
    Width = 473
    Height = 97
    Color = clMenuBar
    DataSource = DataSource1
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object cari: TButton
    Left = 464
    Top = 112
    Width = 49
    Height = 17
    Caption = 'Cari'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    OnClick = cariClick
  end
  object ADOConnection1: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=root;Data' +
      ' Source=uas_provis;Initial Catalog=db_uas_provis'
    LoginPrompt = False
    Left = 80
    Top = 152
  end
  object ADOQuery1: TADOQuery
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select*from t_retur')
    Left = 104
    Top = 192
  end
  object DataSource1: TDataSource
    DataSet = ADOQuery1
    Left = 128
    Top = 152
  end
end
