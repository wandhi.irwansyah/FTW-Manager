unit dashboard;
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, StdCtrls, ExtCtrls, DB, ADODB, ActnList,
  XPStyleActnCtrls, ActnMan;

type
  Tform_dashboard = class(TForm)
    Label1: TLabel;
    Panel1: TPanel;
    b_logout: TButton;
    Panel2: TPanel;
    Label5: TLabel;
    Label6: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    DBGrid1: TDBGrid;
    Panel3: TPanel;
    Label7: TLabel;
    Edit3: TEdit;
    Button4: TButton;
    Panel4: TPanel;
    Button5: TButton;
    DataSource1: TDataSource;
    ADOQuery1: TADOQuery;
    ADOConnection1: TADOConnection;
    Button6: TButton;
    b_input_retur: TButton;
    ActionManager1: TActionManager;
    Action2: TAction;
    Action3: TAction;
    Action4: TAction;
    Action5: TAction;
    Action6: TAction;
    Action7: TAction;
    Action8: TAction;
    Action1: TAction;
    Label8: TLabel;
    Label9: TLabel;
    Edit4: TEdit;
    ADOQuery2: TADOQuery;
    ADOQuery3: TADOQuery;
    ADOQuery4: TADOQuery;
    Edit5: TEdit;
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure b_transaksiClick(Sender: TObject);
    procedure b_input_returClick(Sender: TObject);
    procedure b_logoutClick(Sender: TObject);
  private
    { Private declarations }
  public
  int: String;
  i : Integer;
  trs : Integer;
  index : Integer;

    { Public declarations }
  end;

var
  form_dashboard: Tform_dashboard;

implementation

uses login, input_retur, transaksi, struk;

{$R *.dfm}

procedure Tform_dashboard.Button5Click(Sender: TObject);
var
i : Integer;
j:Double;
begin

  if(index =1)then
    begin



    end
    else
    begin

    int :=formatdatetime('dd/mm/yyyy',now)+formatdatetime('hh:nn:ss',now);
    index :=1;



    end;
  ADOQuery1.Close;
      ADOQuery1.SQL.Clear;
      ADOQuery1.SQL.Text:='Select * from t_d_transaksi where kode_transaksi=''' +int+'''';
      ADOQuery1.Open;
 if(edit1.Text ='')or (edit2.Text ='') then
 begin
  Application.MessageBox('Data tidak boleh kosong','INFORMASI',mb_iconinformation);
 end
 else
  begin
      ADOQuery1.Close;
      ADOQuery1.SQL.Clear;
      ADOQuery1.SQL.Text:='Select * from t_barang where kode_barang=''' +edit1.Text+'''';
      ADOQuery1.Open;
      if ADOQuery1.IsEmpty then
      begin
       Application.MessageBox('Barang tidak tersedia','INFORMASI',mb_iconinformation);
      end
      else
        begin
         ADOQuery3.Close;
      ADOQuery3.SQL.Clear;
      ADOQuery3.SQL.Text:='Select * from t_stock where kode_barang=''' +ADOQuery1['kode_barang']+'''';
      ADOQuery3.Open;
      if(strToInt(ADOQuery3['jumlah'])- strToInt(edit2.Text) >1)then
       begin
          Application.MessageBox('Barang tersedia','INFORMASI',mb_iconinformation);
           Edit4.Text := intToSTR(strToInt(ADOQuery1['harga'])*strToInt(edit2.Text)) ;
           form1.ListBox1.Items.Add(ADOQuery1['kode_barang']);
           form1.ListBox2.Items.Add(edit2.Text);
           form1.ListBox3.Items.Add(intToSTR(strToInt(ADOQuery1['harga'])*strToInt(edit2.Text)));
           edit5.text:= intToStr(strToInt(ADOQuery1['harga'])*strToInt(edit2.Text));
           ADOQuery1.Close;
           ADOQuery1.SQL.Clear;
           ADOQuery1.SQL.Add('INSERT INTO t_d_transaksi VALUES ("'+int+'", "'+Edit1.Text+'", "'+Edit2.Text+'","'+Edit4.Text+'")');
           ADOQuery1.ExecSQL;
           ADOQuery4.Close;
           ADOQuery4.SQL.Clear;
           ADOQuery4.SQL.Add('Update t_stock set jumlah ='''+edit5.text+''' where kode_barang="'+edit1.Text+'" ');
           ADOQuery4.ExecSQL;
          ADOQuery1.SQL.Clear;
          ADOQuery1.SQL.Text:='Select * from t_d_transaksi where kode_transaksi='''+int+'''';
          ADOQuery1.Open;
          ADOQuery2.SQL.Clear;
          ADOQuery2.SQL.Text:='Select *,SUM(harga) AS total from t_d_transaksi where kode_transaksi='''+int+'''';
          ADOQuery2.Open;
          edit3.Text := ADOQuery2['total'];
          form1.Label4.Caption := ADOQuery2['total'];
          form1.edit1.text := int;
          form1.edit1.text := int;

        end
          else
          begin
           Application.MessageBox('Barang Melebihi Stok Yang Ada','INFORMASI',mb_iconinformation);
          end;
        end;
  end;
end;

procedure Tform_dashboard.Button6Click(Sender: TObject);
begin
index :=0;
ADOQuery1.Close;
ADOQuery1.SQL.Clear;
ADOQuery1.SQL.Add('INSERT INTO t_transaksi VALUES ("'+int+'", "'+formatdatetime('yyyy-mm-dd',now)+'", "'+Edit2.Text+'","'+Edit3.Text+'")');
ADOQuery1.ExecSQL;
edit1.Text :='';
edit2.Text :='';
edit3.Text :='';
edit4.Text :='';

form1.show;
end;

procedure Tform_dashboard.b_transaksiClick(Sender: TObject);
begin
  form_transaksi.Show;
end;

procedure Tform_dashboard.b_input_returClick(Sender: TObject);
begin
  form_input_retur.Show;
end;

procedure Tform_dashboard.b_logoutClick(Sender: TObject);
begin
  form_login.Visible := true;
  Close;
end;

end.
