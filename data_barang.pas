unit data_barang;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, ExtCtrls, DBCtrls, StdCtrls, Grids, DBGrids;

type
  Tform_data_barang = class(TForm)
    Label1: TLabel;
    DBGrid1: TDBGrid;
    Button1: TButton;
    Button3: TButton;
    c_barang: TADOConnection;
    q_barang: TADOQuery;
    Label2: TLabel;
    ds_barang: TDataSource;
    ComboBox1: TComboBox;
    e_cari: TEdit;
    Button4: TButton;
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  form_data_barang: Tform_data_barang;

implementation

uses input_artikel;

{$R *.dfm}

procedure Tform_data_barang.Button1Click(Sender: TObject);
begin
 form_input_artikel.Show;
end;

procedure Tform_data_barang.FormShow(Sender: TObject);
begin
  q_barang.Close;
  q_barang.SQL.Clear;
  q_barang.SQL.Append('SELECT a.kode_barang, a.nama_barang, a.brand, a.jenis_barang, a.ukuran, a.warna, a.harga, b.jumlah FROM t_barang a, t_stock b WHERE a.kode_barang = b.kode_barang');
  q_barang.Open;
  
end;

procedure Tform_data_barang.Button4Click(Sender: TObject);
var  hasil : String;
begin
  hasil := ComboBox1.Text;
  q_barang.Close;
  q_barang.SQL.Clear;
  q_barang.SQL.Append('SELECT a.kode_barang, a.nama_barang, a.brand, a.jenis_barang, a.ukuran, a.warna, a.harga, b.jumlah FROM t_barang a, t_stock b WHERE a.kode_barang = b.kode_barang AND a.'+hasil+' LIKE "%'+e_cari.Text+'%"');
  q_barang.Open;
end;

end.
