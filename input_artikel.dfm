object form_input_artikel: Tform_input_artikel
  Left = 365
  Top = 114
  Width = 869
  Height = 480
  Caption = 'form_input_artikel'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 160
    Top = 72
    Width = 308
    Height = 16
    Caption = 'Digunakan untuk menambahkan barang baru'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object StaticText1: TStaticText
    Left = 160
    Top = 24
    Width = 415
    Height = 43
    Caption = 'Input Artikel Barang Baru'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -32
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
  end
  object GroupBox1: TGroupBox
    Left = 160
    Top = 96
    Width = 497
    Height = 297
    Caption = 'Masukan Data Barang'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    object Label3: TLabel
      Left = 248
      Top = 184
      Width = 93
      Height = 16
      Caption = 'Jumlah          : '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 40
      Top = 40
      Width = 94
      Height = 16
      Caption = 'Nama Barang :'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 40
      Top = 88
      Width = 95
      Height = 16
      Caption = 'Brand             :'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 248
      Top = 88
      Width = 91
      Height = 16
      Caption = 'Jenis Barang :'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 40
      Top = 128
      Width = 94
      Height = 16
      Caption = 'Ukuran           :'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 248
      Top = 128
      Width = 92
      Height = 16
      Caption = 'Warna           :'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 456
      Top = 184
      Width = 22
      Height = 16
      Caption = 'Pcs'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 40
      Top = 184
      Width = 88
      Height = 16
      Caption = 'Harga           :'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
    end
    object EJumlah: TEdit
      Left = 352
      Top = 176
      Width = 97
      Height = 27
      TabOrder = 0
    end
    object BSave: TBitBtn
      Left = 40
      Top = 224
      Width = 65
      Height = 25
      Caption = 'Save'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = BSaveClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333330070
        7700333333337777777733333333008088003333333377F73377333333330088
        88003333333377FFFF7733333333000000003FFFFFFF77777777000000000000
        000077777777777777770FFFFFFF0FFFFFF07F3333337F3333370FFFFFFF0FFF
        FFF07F3FF3FF7FFFFFF70F00F0080CCC9CC07F773773777777770FFFFFFFF039
        99337F3FFFF3F7F777F30F0000F0F09999937F7777373777777F0FFFFFFFF999
        99997F3FF3FFF77777770F00F000003999337F773777773777F30FFFF0FF0339
        99337F3FF7F3733777F30F08F0F0337999337F7737F73F7777330FFFF0039999
        93337FFFF7737777733300000033333333337777773333333333}
      NumGlyphs = 2
    end
    object BClose: TBitBtn
      Left = 384
      Top = 216
      Width = 65
      Height = 25
      Caption = 'Close'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = BCloseClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333333333333333333333333FFF33FF333FFF339993370733
        999333777FF37FF377733339993000399933333777F777F77733333399970799
        93333333777F7377733333333999399933333333377737773333333333990993
        3333333333737F73333333333331013333333333333777FF3333333333910193
        333333333337773FF3333333399000993333333337377737FF33333399900099
        93333333773777377FF333399930003999333337773777F777FF339993370733
        9993337773337333777333333333333333333333333333333333333333333333
        3333333333333333333333333333333333333333333333333333}
      NumGlyphs = 2
    end
    object ENamaBarang: TEdit
      Left = 144
      Top = 40
      Width = 305
      Height = 27
      TabOrder = 3
    end
    object EBrand: TEdit
      Left = 144
      Top = 88
      Width = 97
      Height = 27
      TabOrder = 4
    end
    object EJenisBarang: TEdit
      Left = 352
      Top = 88
      Width = 97
      Height = 27
      TabOrder = 5
    end
    object EUkuran: TEdit
      Left = 144
      Top = 128
      Width = 97
      Height = 27
      TabOrder = 6
    end
    object EWarna: TEdit
      Left = 352
      Top = 128
      Width = 97
      Height = 27
      TabOrder = 7
    end
    object EHarga: TEdit
      Left = 144
      Top = 176
      Width = 97
      Height = 27
      TabOrder = 8
    end
  end
  object ADOConnection1: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=root;Data' +
      ' Source=uas_provis;Initial Catalog=db_uas_provis'
    LoginPrompt = False
    Left = 96
    Top = 120
  end
  object ado_t_barang: TADOTable
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    TableName = 't_barang'
    Left = 96
    Top = 160
    object ado_t_barangkode_barang: TStringField
      FieldName = 'kode_barang'
    end
    object ado_t_barangnama_barang: TStringField
      FieldName = 'nama_barang'
    end
    object ado_t_barangbrand: TStringField
      FieldName = 'brand'
    end
    object ado_t_barangjenis_barang: TStringField
      FieldName = 'jenis_barang'
    end
    object ado_t_barangukuran: TIntegerField
      FieldName = 'ukuran'
    end
    object ado_t_barangwarna: TStringField
      FieldName = 'warna'
    end
    object ado_t_barangharga: TIntegerField
      FieldName = 'harga'
    end
  end
  object ado_t_stock: TADOTable
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    TableName = 't_stock'
    Left = 96
    Top = 216
    object ado_t_stockkode_barang: TStringField
      FieldName = 'kode_barang'
    end
    object ado_t_stockjumlah: TIntegerField
      FieldName = 'jumlah'
    end
  end
end
