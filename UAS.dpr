program UAS;

uses
  Forms,
  login in 'login.pas' {form_login},
  dashboard in 'dashboard.pas' {form_dashboard},
  input_stock_barang in 'input_stock_barang.pas' {form_input_stock_barang},
  input_artikel in 'input_artikel.pas' {form_input_artikel},
  input_user in 'input_user.pas' {form_input_user},
  input_retur in 'input_retur.pas' {form_input_retur},
  data_user in 'data_user.pas' {form_data_user},
  data_barang in 'data_barang.pas' {form_data_barang},
  data_retur in 'data_retur.pas' {form_data_retur},
  dashboard_master in 'dashboard_master.pas' {form_dashboard_master},
  struk in 'struk.pas' {Form1};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(Tform_input_user, form_input_user);
  Application.CreateForm(Tform_input_retur, form_input_retur);
  Application.CreateForm(Tform_data_user, form_data_user);
  Application.CreateForm(Tform_login, form_login);
  Application.CreateForm(Tform_data_retur, form_data_retur);
  Application.CreateForm(Tform_input_artikel, form_input_artikel);
  Application.CreateForm(Tform_login, form_login);
  Application.CreateForm(Tform_input_artikel, form_input_artikel);
  Application.CreateForm(Tform_data_barang, form_data_barang);
  Application.CreateForm(Tform_input_stock_barang, form_input_stock_barang);
  Application.CreateForm(Tform_data_barang, form_data_barang);
  Application.CreateForm(Tform_dashboard, form_dashboard);
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(Tform_dashboard_master, form_dashboard_master);
  Application.Run;
end.
