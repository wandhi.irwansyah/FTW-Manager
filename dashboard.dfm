object form_dashboard: Tform_dashboard
  Left = 119
  Top = 127
  Width = 1216
  Height = 478
  Caption = 'form_dashboard'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 320
    Top = 16
    Width = 32
    Height = 13
    Caption = 'Label1'
  end
  object Panel1: TPanel
    Left = -17
    Top = 0
    Width = 1209
    Height = 57
    TabOrder = 0
    object Label8: TLabel
      Left = 144
      Top = 24
      Width = 99
      Height = 19
      Caption = '11123121231'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label9: TLabel
      Left = 64
      Top = 24
      Width = 71
      Height = 19
      Caption = 'Username'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object b_logout: TButton
      Left = 1048
      Top = 16
      Width = 105
      Height = 25
      Caption = 'Logout'
      TabOrder = 0
      OnClick = b_logoutClick
    end
    object b_input_retur: TButton
      Left = 936
      Top = 16
      Width = 105
      Height = 25
      Caption = 'Input Retur'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = b_input_returClick
    end
  end
  object Panel2: TPanel
    Left = 536
    Top = 104
    Width = 593
    Height = 41
    TabOrder = 1
    object Label5: TLabel
      Left = 8
      Top = 8
      Width = 46
      Height = 19
      Caption = 'Artikel'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 288
      Top = 8
      Width = 51
      Height = 19
      Caption = 'Jumlah'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Edit1: TEdit
      Left = 64
      Top = 8
      Width = 121
      Height = 21
      TabOrder = 0
    end
    object Edit2: TEdit
      Left = 360
      Top = 8
      Width = 121
      Height = 21
      TabOrder = 1
    end
  end
  object DBGrid1: TDBGrid
    Left = 39
    Top = 168
    Width = 1089
    Height = 169
    DataSource = DataSource1
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object Panel3: TPanel
    Left = 895
    Top = 352
    Width = 233
    Height = 41
    TabOrder = 3
    object Label7: TLabel
      Left = 8
      Top = 8
      Width = 36
      Height = 19
      Caption = 'Total'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Edit3: TEdit
      Left = 80
      Top = 8
      Width = 121
      Height = 21
      TabOrder = 0
    end
  end
  object Button4: TButton
    Left = 264
    Top = 352
    Width = 113
    Height = 33
    Caption = 'DELETE'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
  end
  object Panel4: TPanel
    Left = 31
    Top = 424
    Width = 1154
    Height = 25
    Color = clBackground
    TabOrder = 5
  end
  object Button5: TButton
    Left = 1040
    Top = 112
    Width = 75
    Height = 25
    Caption = 'Tambah'
    TabOrder = 6
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 392
    Top = 352
    Width = 113
    Height = 33
    Caption = 'SELESAI'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 7
    OnClick = Button6Click
  end
  object Edit4: TEdit
    Left = 40
    Top = 136
    Width = 121
    Height = 21
    ParentShowHint = False
    ShowHint = True
    TabOrder = 8
  end
  object Edit5: TEdit
    Left = 176
    Top = 136
    Width = 121
    Height = 21
    ParentShowHint = False
    ShowHint = True
    TabOrder = 9
  end
  object DataSource1: TDataSource
    DataSet = ADOQuery1
    Left = 152
    Top = 368
  end
  object ADOQuery1: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    Left = 112
    Top = 368
  end
  object ADOConnection1: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=root;Data' +
      ' Source=uas_provis;Initial Catalog=db_uas_provis'
    LoginPrompt = False
    Left = 72
    Top = 368
  end
  object ActionManager1: TActionManager
    ActionBars = <
      item
        Items = <
          item
            Action = Action8
            Caption = '&Logout'
          end
          item
            Items = <
              item
                Action = Action5
                Caption = '&Stock Barang'
              end
              item
                Action = Action6
                Caption = '&User'
              end
              item
                Action = Action7
                Caption = '&Artikel'
              end>
            Caption = '&Input'
          end
          item
            Items = <
              item
                Action = Action2
                Caption = '&Data Stock Barang'
              end
              item
                Action = Action3
                Caption = 'D&ata User'
              end
              item
                Action = Action4
                Caption = 'Da&ta Retur'
              end
              item
                Action = Action1
                Caption = 'Data T&ransaksi'
              end>
            Caption = '&Report'
          end>
      end>
    Left = 248
    Top = 16
    StyleName = 'XP Style'
    object Action2: TAction
      Category = 'Report'
      Caption = 'Data Stock Barang'
    end
    object Action3: TAction
      Category = 'Report'
      Caption = 'Data User'
    end
    object Action4: TAction
      Category = 'Report'
      Caption = 'Data Retur'
    end
    object Action5: TAction
      Category = 'Input'
      Caption = 'Stock Barang'
    end
    object Action6: TAction
      Category = 'Input'
      Caption = 'User'
    end
    object Action7: TAction
      Category = 'Input'
      Caption = 'Artikel'
    end
    object Action8: TAction
      Caption = 'Logout'
    end
    object Action1: TAction
      Category = 'Report'
      Caption = 'Data Transaksi'
    end
  end
  object ADOQuery2: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    Left = 192
    Top = 368
  end
  object ADOQuery3: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    Left = 224
    Top = 368
  end
  object ADOQuery4: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    Left = 224
    Top = 400
  end
end
