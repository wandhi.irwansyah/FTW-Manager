object form_input_stock_barang: Tform_input_stock_barang
  Left = 323
  Top = 115
  Width = 541
  Height = 538
  Caption = 'form_input_stock_barang'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 32
    Top = 64
    Width = 298
    Height = 16
    Caption = 'Digunakan untuk penginputan stock barang'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object StaticText1: TStaticText
    Left = 32
    Top = 16
    Width = 406
    Height = 43
    Caption = 'Form Input Stock Barang'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -32
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
  end
  object GroupBox1: TGroupBox
    Left = 32
    Top = 88
    Width = 465
    Height = 377
    Caption = 'Masukan Data Barang'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    object Label2: TLabel
      Left = 40
      Top = 48
      Width = 99
      Height = 16
      Caption = 'Kode Barang  : '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 40
      Top = 288
      Width = 97
      Height = 16
      Caption = 'Jumlah           : '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 416
      Top = 288
      Width = 22
      Height = 16
      Caption = 'Pcs'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
    end
    object TLabel
      Left = 40
      Top = 88
      Width = 98
      Height = 16
      Caption = 'Nama Barang : '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 40
      Top = 128
      Width = 99
      Height = 16
      Caption = 'Brand             : '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 40
      Top = 168
      Width = 99
      Height = 16
      Caption = 'Jenis Barang  : '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 40
      Top = 208
      Width = 100
      Height = 16
      Caption = 'Warna            : '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 40
      Top = 248
      Width = 100
      Height = 16
      Caption = 'Harga             : '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
    end
    object EJumlah: TEdit
      Left = 272
      Top = 288
      Width = 137
      Height = 27
      TabOrder = 0
    end
    object BSave: TBitBtn
      Left = 344
      Top = 328
      Width = 65
      Height = 25
      Caption = 'Save'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = BSaveClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333330070
        7700333333337777777733333333008088003333333377F73377333333330088
        88003333333377FFFF7733333333000000003FFFFFFF77777777000000000000
        000077777777777777770FFFFFFF0FFFFFF07F3333337F3333370FFFFFFF0FFF
        FFF07F3FF3FF7FFFFFF70F00F0080CCC9CC07F773773777777770FFFFFFFF039
        99337F3FFFF3F7F777F30F0000F0F09999937F7777373777777F0FFFFFFFF999
        99997F3FF3FFF77777770F00F000003999337F773777773777F30FFFF0FF0339
        99337F3FF7F3733777F30F08F0F0337999337F7737F73F7777330FFFF0039999
        93337FFFF7737777733300000033333333337777773333333333}
      NumGlyphs = 2
    end
    object CBKodeBarang: TComboBox
      Left = 144
      Top = 48
      Width = 265
      Height = 27
      ItemHeight = 19
      TabOrder = 2
      OnChange = CBKodeBarangChange
    end
    object enama: TEdit
      Left = 168
      Top = 88
      Width = 241
      Height = 27
      Enabled = False
      TabOrder = 3
    end
    object ebrand: TEdit
      Left = 168
      Top = 128
      Width = 241
      Height = 27
      Enabled = False
      TabOrder = 4
    end
    object ejenis: TEdit
      Left = 168
      Top = 168
      Width = 241
      Height = 27
      Enabled = False
      TabOrder = 5
    end
    object ewarna: TEdit
      Left = 168
      Top = 208
      Width = 241
      Height = 27
      Enabled = False
      TabOrder = 6
    end
    object eharga: TEdit
      Left = 168
      Top = 248
      Width = 241
      Height = 27
      Enabled = False
      TabOrder = 7
    end
  end
  object Q_barang: TADOQuery
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select*from t_barang')
    Left = 8
    Top = 112
  end
  object ADOConnection1: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=root;Data' +
      ' Source=uas_provis;Initial Catalog=db_uas_provis'
    LoginPrompt = False
    Left = 8
    Top = 80
  end
  object q_stock: TADOQuery
    Active = True
    AutoCalcFields = False
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select*from t_stock')
    Left = 8
    Top = 152
  end
  object ado_barang: TADOTable
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    TableName = 't_barang'
    Left = 8
    Top = 192
    object ado_barangkode_barang: TStringField
      FieldName = 'kode_barang'
    end
    object ado_barangnama_barang: TStringField
      FieldName = 'nama_barang'
    end
    object ado_barangbrand: TStringField
      FieldName = 'brand'
    end
    object ado_barangjenis_barang: TStringField
      FieldName = 'jenis_barang'
    end
    object ado_barangukuran: TIntegerField
      FieldName = 'ukuran'
    end
    object ado_barangwarna: TStringField
      FieldName = 'warna'
    end
    object ado_barangharga: TIntegerField
      FieldName = 'harga'
    end
  end
  object ado_stock: TADOTable
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    TableName = 't_stock'
    Left = 8
    Top = 232
    object ado_stockkode_barang: TStringField
      FieldName = 'kode_barang'
    end
    object ado_stockjumlah: TIntegerField
      FieldName = 'jumlah'
    end
  end
end
