object form_input_user: Tform_input_user
  Left = 275
  Top = 143
  Width = 557
  Height = 451
  Caption = 'form_input_user'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 40
    Top = 56
    Width = 353
    Height = 16
    Caption = 'Digunakan untuk menambahkan data pegawai baru'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object StaticText1: TStaticText
    Left = 40
    Top = 16
    Width = 461
    Height = 43
    Caption = 'INPUT DATA KEPEGAWAIAN'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -32
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
  end
  object GroupBox1: TGroupBox
    Left = 40
    Top = 80
    Width = 457
    Height = 297
    Caption = 'Masukan Data'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    object Label2: TLabel
      Left = 40
      Top = 64
      Width = 133
      Height = 16
      Caption = 'NAMA PEGAWAI      :'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 40
      Top = 104
      Width = 133
      Height = 16
      Caption = 'Alamat Pegawai      :'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 40
      Top = 144
      Width = 133
      Height = 16
      Caption = 'NoTelepon Pegawai :'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 40
      Top = 224
      Width = 132
      Height = 16
      Caption = 'Password                :'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
    end
    object SpeedButton1: TSpeedButton
      Left = 360
      Top = 24
      Width = 65
      Height = 22
      Glyph.Data = {
        E6000000424DE60000000000000076000000280000000E0000000E0000000100
        0400000000007000000000000000000000001000000000000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3300333333333333330033333333333333003333333333333300333333333333
        330033333333333333003300000000003300330FFFFFFFF03300330000000000
        3300333333333333330033333333333333003333333333333300333333333333
        33003333333333333300}
      OnClick = SpeedButton1Click
    end
    object Label6: TLabel
      Left = 40
      Top = 184
      Width = 133
      Height = 16
      Caption = 'Level  Pegawai        :'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
    end
    object BitBtn1: TBitBtn
      Left = 360
      Top = 256
      Width = 65
      Height = 25
      Caption = 'Save'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BitBtn1Click
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333330070
        7700333333337777777733333333008088003333333377F73377333333330088
        88003333333377FFFF7733333333000000003FFFFFFF77777777000000000000
        000077777777777777770FFFFFFF0FFFFFF07F3333337F3333370FFFFFFF0FFF
        FFF07F3FF3FF7FFFFFF70F00F0080CCC9CC07F773773777777770FFFFFFFF039
        99337F3FFFF3F7F777F30F0000F0F09999937F7777373777777F0FFFFFFFF999
        99997F3FF3FFF77777770F00F000003999337F773777773777F30FFFF0FF0339
        99337F3FF7F3733777F30F08F0F0337999337F7737F73F7777330FFFF0039999
        93337FFFF7737777733300000033333333337777773333333333}
      NumGlyphs = 2
    end
    object e_nama: TEdit
      Left = 232
      Top = 64
      Width = 193
      Height = 27
      TabOrder = 1
    end
    object e_alamat: TEdit
      Left = 232
      Top = 104
      Width = 193
      Height = 27
      TabOrder = 2
    end
    object e_no_telp: TEdit
      Left = 232
      Top = 144
      Width = 193
      Height = 27
      TabOrder = 3
    end
    object e_pass: TEdit
      Left = 232
      Top = 224
      Width = 193
      Height = 27
      PasswordChar = '*'
      TabOrder = 4
    end
    object CBlvl: TComboBox
      Left = 232
      Top = 184
      Width = 193
      Height = 27
      ItemHeight = 19
      TabOrder = 5
      Items.Strings = (
        'Admin'
        'SPV')
    end
  end
  object c_input_user: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=root;Data' +
      ' Source=uas_provis;Initial Catalog=db_uas_provis'
    LoginPrompt = False
    Top = 72
  end
  object q_input_user: TADOQuery
    Connection = c_input_user
    Parameters = <>
    Top = 104
  end
  object t_input_user: TADOTable
    Active = True
    Connection = c_input_user
    CursorType = ctStatic
    TableName = 't_user'
    Top = 160
    object t_input_usernik: TStringField
      FieldName = 'nik'
    end
    object t_input_usernama: TStringField
      FieldName = 'nama'
    end
    object t_input_useralamat: TStringField
      FieldName = 'alamat'
      Size = 50
    end
    object t_input_usertelepon: TStringField
      FieldName = 'telepon'
    end
    object t_input_userlevel: TIntegerField
      FieldName = 'level'
    end
    object t_input_userstatus: TIntegerField
      FieldName = 'status'
    end
    object t_input_userpassword: TStringField
      FieldName = 'password'
    end
  end
end
