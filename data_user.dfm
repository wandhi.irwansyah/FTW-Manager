object form_data_user: Tform_data_user
  Left = 188
  Top = 89
  Width = 986
  Height = 558
  Caption = 'form_data_user'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object nik: TLabel
    Left = 128
    Top = 96
    Width = 84
    Height = 13
    Caption = 'Search By NIK :'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label1: TLabel
    Left = 128
    Top = 40
    Width = 164
    Height = 29
    Caption = 'FTW Manager'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 128
    Top = 72
    Width = 123
    Height = 13
    Caption = 'Tabel Data Login User'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Enik: TEdit
    Left = 216
    Top = 96
    Width = 145
    Height = 21
    TabOrder = 0
  end
  object GroupBox1: TGroupBox
    Left = 32
    Top = 128
    Width = 873
    Height = 281
    Caption = 'Dashboard Data User'
    Color = clGradientActiveCaption
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 1
    object DBGrid1: TDBGrid
      Left = 0
      Top = 32
      Width = 873
      Height = 209
      DataSource = DataSource1
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Verdana'
      TitleFont.Style = [fsBold]
      Columns = <
        item
          Expanded = False
          FieldName = 'nik'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'nama'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'alamat'
          Width = 98
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'telepon'
          Width = 112
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'level'
          Width = 35
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'status'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'password'
          Visible = True
        end>
    end
    object BitBtn1: TBitBtn
      Left = 784
      Top = 248
      Width = 81
      Height = 25
      Caption = 'Tambah'
      TabOrder = 1
      OnClick = BitBtn1Click
      Glyph.Data = {
        42040000424D4204000000000000420000002800000010000000100000000100
        20000300000000040000000000000000000000000000000000000000FF0000FF
        0000FF0000000000000000000000000000000000000000000000C2632EBFC465
        2FFFC76730FFCA6A30FFCD6C31FFD06E32BF0000000000000000000000000000
        0000000000000000000000000000000000000000000000000000C1632EFFF4BD
        86FFEE9C4FFFEE9641FFF19C47FFD06E32FF0000000000000000000000000000
        0000000000000000000000000000000000000000000000000000C1622EFFF4BD
        87FFE8934FFFE98F41FFF29E49FFCF6E32FF0000000000000000000000000000
        0000000000000000000000000000000000000000000000000000C0622EFFF4BD
        87FFE68F4EFFE88B41FFF29E48FFCE6D31FF0000000000000000000000000000
        0000000000000000000000000000000000000000000000000000C0612DFFF4BC
        86FFE68E4DFFE7893FFFF19C46FFCD6C31FF0000000000000000000000000000
        000000000000B05629BFB3582AFFB65A2BFFB95C2BFFBC5E2CFFBE602DFFF3BC
        86FFE48B4BFFE5863DFFEF9843FFCB6A30FFCD6C31FFCE6D31FFCF6E32FFD06E
        32FFD06E32DFAF5529FFF0B98AFFE69353FFE38740FFE38236FFE38232FFE993
        49FFE07E3AFFE38139FFED943FFFEF9943FFF19C46FFF29E48FFF29E49FFF19D
        47FFCD6C31FFAE5428FFF0B98AFFDD834CFFD87339FFDA763AFFDB783AFFDC77
        36FFDB7433FFDE7835FFE38139FFE5863DFFE78940FFE88C41FFEA8F42FFEE96
        41FFCA6A30FFAD5328FFF0B98BFFDF8C5AFFDA7E4BFFDC814CFFDE844EFFE087
        4FFFDD7D42FFDE7E40FFE28644FFE69154FFE79254FFE89455FFEA9856FFEFA1
        57FFC76730FFAC5228FFEFB98BFFF0B98BFFF1BA8AFFF1BB8BFFF1BC8BFFF2BD
        8BFFE08750FFDE7E3FFFEB9D57FFF4C08DFFF5C18DFFF5C18DFFF5C18DFFF5C1
        8DFFC4652FFFAA5127BFAD5228FFAF5428FFB15629FFB3582AFFB65A2AFFF1BC
        8BFFDE844EFFDC7A3DFFE68E44FFBE602DFFC0612DFFC0622EFFC1622EFFC163
        2EFFC2632EA60000000000000000000000000000000000000000B3582AFFF1BB
        8BFFDC814DFFDA763BFFE58B42FFBC5E2CFF0000000000000000000000000000
        0000000000000000000000000000000000000000000000000000B15629FFF1BA
        8BFFDB7F4BFFD8733AFFE38942FFB95C2BFF0000000000000000000000000000
        0000000000000000000000000000000000000000000000000000AF5428FFF0BA
        8BFFDF8C5BFFDD834DFFE69453FFB65A2BFF0000000000000000000000000000
        0000000000000000000000000000000000000000000000000000AD5228FFF0BA
        8BFFF0BA8BFFF0BA8BFFF0BA8BFFB3582AFF0000000000000000000000000000
        0000000000000000000000000000000000000000000000000000AA5127BFAC52
        28FFAD5328FFAE5428FFAF5529FFB05629BF0000000000000000000000000000
        000000000000}
    end
  end
  object ComboBox1: TComboBox
    Left = 792
    Top = 96
    Width = 81
    Height = 21
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ItemHeight = 13
    ParentFont = False
    TabOrder = 2
    Text = 'Nik'
    Items.Strings = (
      'nik'
      'telepon'
      'alamat'
      'nama')
  end
  object Button1: TButton
    Left = 368
    Top = 104
    Width = 49
    Height = 9
    Caption = 'Cari'
    TabOrder = 3
    OnClick = Button1Click
  end
  object Button4: TButton
    Left = 720
    Top = 96
    Width = 49
    Height = 25
    Caption = 'Sort By'
    TabOrder = 4
    OnClick = Button4Click
  end
  object ADOConnection1: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=root;Data' +
      ' Source=uas_provis;Initial Catalog=db_uas_provis'
    LoginPrompt = False
    Left = 144
    Top = 472
  end
  object ADOlogin: TADOQuery
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select*from t_user')
    Left = 184
    Top = 472
  end
  object DataSource1: TDataSource
    DataSet = ADOlogin
    Left = 224
    Top = 480
  end
  object ADOTable1: TADOTable
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    TableName = 't_user'
    Left = 472
    Top = 72
    object ADOTable1nik: TStringField
      FieldName = 'nik'
    end
    object ADOTable1nama: TStringField
      FieldName = 'nama'
    end
    object ADOTable1alamat: TStringField
      FieldName = 'alamat'
      Size = 50
    end
    object ADOTable1telepon: TStringField
      FieldName = 'telepon'
    end
    object ADOTable1level: TIntegerField
      FieldName = 'level'
    end
    object ADOTable1status: TIntegerField
      FieldName = 'status'
    end
    object ADOTable1password: TStringField
      FieldName = 'password'
    end
  end
end
