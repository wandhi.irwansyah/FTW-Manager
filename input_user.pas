unit input_user;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, ExtCtrls, DBCtrls, StdCtrls, Grids, DBGrids, Buttons;

type
  Tform_input_user = class(TForm)
    Label1: TLabel;
    StaticText1: TStaticText;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    SpeedButton1: TSpeedButton;
    BitBtn1: TBitBtn;
    e_nama: TEdit;
    e_alamat: TEdit;
    e_no_telp: TEdit;
    e_pass: TEdit;
    c_input_user: TADOConnection;
    q_input_user: TADOQuery;
    t_input_user: TADOTable;
    t_input_usernik: TStringField;
    t_input_usernama: TStringField;
    t_input_useralamat: TStringField;
    t_input_usertelepon: TStringField;
    t_input_userlevel: TIntegerField;
    t_input_userstatus: TIntegerField;
    t_input_userpassword: TStringField;
    Label6: TLabel;
    CBlvl: TComboBox;
    procedure BitBtn1Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  form_input_user: Tform_input_user;

implementation

uses StrUtils;

{$R *.dfm}

procedure Tform_input_user.BitBtn1Click(Sender: TObject);
var
  nik, nama, angka, lvl, tgl : string;
begin
  case CBlvl.ItemIndex of
    0 : lvl := '0';
    1 : lvl := '1';
  end;
  angka := MidStr(e_no_telp.Text, 4,3);
  nama := LeftStr(e_nama.Text, 3);
  tgl := (FormatDateTime('yyyymmdd', now));
  nik := nama + angka + tgl;
  q_input_user.Close;
  q_input_user.SQL.Clear;
  q_input_user.SQL.Add('INSERT INTO t_user VALUES ("'+nik+'", "'+e_nama.Text+'", "'+e_alamat.Text+'", "'+e_no_telp.Text+'", "'+lvl+'", "1", "'+e_pass.Text+'")');
  q_input_user.ExecSQL;
  t_input_user.Open;
  e_nama.Clear;
  e_alamat.Clear;
  e_no_telp.Clear;
  e_pass.Clear;
  e_nama.SetFocus;

  ShowMessage('DATA TELAH TERINPUT TERIMAKASIH!');
 close;
end;

procedure Tform_input_user.SpeedButton1Click(Sender: TObject);
begin
  e_nama.Clear;
  e_alamat.Clear;
  e_no_telp.Clear;
  e_pass.Clear;
  Close;
end;


end.
